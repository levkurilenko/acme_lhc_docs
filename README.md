# ACME LHC Documentation 
 
### Folder Descriptions  
**git**: description of various git repositories in use  
**kc705**: Xilinx KC705 board schematic and documentation  
**rd53**: RD53A chip spec, Aurora protocol, undergraduate project documentation  
**thesis**: Thesis reports by previous graduate students  
**tutorials**: ila, modelsim, and vivado tutorials  

